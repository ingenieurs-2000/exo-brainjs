// Importer les données d'entrainement
const trainData = require('./data/train.json');

// Importer Brain.js
const Brain = require('brain.js');

// Importer fs pour créer un fichier
const fs = require('fs');

// Création du réseau de neurones
const NeuralNetwork = new Brain.NeuralNetwork({
    activation: 'sigmoid',
    hiddenLayers: [2],
    iterations: 200000,
    learningRate: 0.5
});

// Entrainer le réseau de neurones
NeuralNetwork.train( trainData, {
    log: true,
    logPeriod: 1000,
    errorThresh: 0.002
})

// Enregistrement du modèle dans un dossier "net" exploitable en NodeJS
fs.writeFileSync(
    'net/trained-model-node.js', 
    `module.exports = ${ NeuralNetwork.toFunction().toString() };`
);

// Enregistrement du modèle dans un dossier "net" exploitable en FrontEnd
fs.writeFileSync(
    'net/trained-model-front.js', 
    `export default ${ NeuralNetwork.toFunction().toString() };`
);