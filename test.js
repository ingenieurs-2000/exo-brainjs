// Importer le modèle
const trainedModel = require('./net/trained-model-node.js');

// Importer les données de test
const testData = require('./data/test.json');

// Utilser le modèle
for( let item of testData ){
    const testedData = trainedModel( item.input );

    console.log('Result', testedData)
    console.log('Output', item.output)
    console.log('\n')
}